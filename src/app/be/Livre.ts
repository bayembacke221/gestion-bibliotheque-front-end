export interface Livre extends Document{
  id:number;
  auteur:string;
  nbrPages:number;
  typeLivre: string;
  numEnreg:number;
  titre:string;
}
