export interface Revue extends Document{
  id:number;
  mois:number;
  numEnreg:number;
  titre:string;
  annee:number;
}
