import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DictionnaireComponent } from './component/dictionnaire/dictionnaire.component';
import { LivreComponent } from './component/livre/livre.component';
import { RevueComponent } from './component/revue/revue.component';

const routes: Routes = [
  { path: '', redirectTo: '/livre', pathMatch: 'full' },
  { path: 'dictionnaire', component: DictionnaireComponent},
  { path: 'livre', component: LivreComponent},
  { path: 'revue', component: RevueComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
