import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { DocumentComponent } from './component/document/document.component';
import { DictionnaireComponent } from './component/dictionnaire/dictionnaire.component';
import { LivreComponent } from './component/livre/livre.component';
import { RevueComponent } from './component/revue/revue.component';

@NgModule({
  declarations: [
    AppComponent,
    DocumentComponent,
    DictionnaireComponent,
    LivreComponent,
    RevueComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule, FormsModule,AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
