import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  public apiServerUrl=environment.apiBaseUrl;
  constructor(private http: HttpClient) { }

  public getDocument():Observable<Document[]>{
    return this.http.get<Document[]>(`${this.apiServerUrl}/bu`);
  }

}

