import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from "src/environments/environment";
import { Dictionnaire } from '../be/Dictionnaire';

@Injectable({
  providedIn: 'root'
})
export class DictionnaireService {
  public apiServerUrl=environment.apiBaseUrl;
  constructor(private http: HttpClient) { }

  public getDictionnaire():Observable<Dictionnaire[]>{
    return this.http.get<Dictionnaire[]>(`${this.apiServerUrl}/dictionnaire`);
  }

  public addDictionnaire(dictionnaire:Dictionnaire):Observable<Dictionnaire>{
      return this.http.post<Dictionnaire>(`${this.apiServerUrl}/dictionnaire`,dictionnaire);
  }

  public getDictionnaireByLanguage(langue:string):Observable<Dictionnaire[]>{
    return this.http.get<Dictionnaire[]>(`${this.apiServerUrl}/dictionnaire/language/${langue}`);
  }

  public updateDictionnaire(dictionnaire:Dictionnaire):Observable<Dictionnaire>{
    return this.http.put<Dictionnaire>(`${this.apiServerUrl}/dictionnaire`,dictionnaire);
  }

  public deleteDictionnaire(id:number):Observable<void>{
    return this.http.delete<void>(`${this.apiServerUrl}/dictionnaire/${id}`);
  }

}
