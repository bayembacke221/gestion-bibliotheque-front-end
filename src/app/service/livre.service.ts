import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from "src/environments/environment";
import { Livre } from '../be/Livre';

@Injectable({
  providedIn: 'root'
})
export class LivreService {
  public apiServerUrl=environment.apiBaseUrl;

  constructor(private http: HttpClient) { }


  public getLivre():Observable<Livre[]>{
    return this.http.get<Livre[]>(`${this.apiServerUrl}/livre`);
  }

  public addLivre(livre:Livre):Observable<Livre>{
    return this.http.post<Livre>(`${this.apiServerUrl}/livre`,livre);
  }

  public getLivreByAuthor(author:string):Observable<Livre[]>{
    return this.http.get<Livre[]>(`${this.apiServerUrl}/livre/author/${author}`);
  }
  public updateLivre(livre:Livre):Observable<Livre>{
    return this.http.put<Livre>(`${this.apiServerUrl}/livre`,livre);
  }

  public deleteLivre(id:number):Observable<void>{
    return this.http.delete<void>(`${this.apiServerUrl}/livre/${id}`);
  }
}
