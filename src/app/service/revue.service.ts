import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from "src/environments/environment";
import { Revue } from '../be/Revue';

@Injectable({
  providedIn: 'root'
})
export class RevueService {

  public apiServerUrl=environment.apiBaseUrl;
  constructor(private http: HttpClient) { }

  public getRevue():Observable<Revue[]>{
    return this.http.get<Revue[]>(`${this.apiServerUrl}/revue`);
  }

  public addRevue(revue:Revue):Observable<Revue>{
    return this.http.post<Revue>(`${this.apiServerUrl}/revue`,revue);
  }

  public getRevueByDate(date:number):Observable<Revue[]>{
    return this.http.get<Revue[]>(`${this.apiServerUrl}/revue/date/${date}`);
  }
  public updateRevue(revue:Revue):Observable<Revue>{
    return this.http.put<Revue>(`${this.apiServerUrl}/revue`,revue);
  }
  public deleteRevue(id:number):Observable<void>{
    return this.http.delete<void>(`${this.apiServerUrl}/revue/${id}`);
  }


}
