import { Component, OnInit } from '@angular/core';
import { DocumentService } from 'src/app/service/document.service';

import {HttpErrorResponse} from "@angular/common/http";
@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.css']
})
export class DocumentComponent implements OnInit{
  public documents: Document[];

  constructor(private documentService:DocumentService){}
  ngOnInit(): void {
    this.getDocument();
  }

  getDocument(): void {
    this.documentService.getDocument().subscribe(
      (response:Document[])=>{
        this.documents = response
        console.log(response);
      },
      (error:HttpErrorResponse)=>{
        alert(error.message)
      }
    )
  }


}
