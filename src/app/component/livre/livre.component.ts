import {Component, OnInit} from '@angular/core';
import {Livre} from "../../be/Livre";
import {LivreService} from "../../service/livre.service";
import {Dictionnaire} from "../../be/Dictionnaire";
import {HttpErrorResponse} from "@angular/common/http";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-livre',
  templateUrl: './livre.component.html',
  styleUrls: ['./livre.component.css']
})
export class LivreComponent implements OnInit{
  livres:Livre[];
  editLivre:Livre;
  deleteLivre:Livre;

  constructor(private livreService:LivreService) {
  }
  ngOnInit(): void {
    this.getLivres();
  }

  getLivres():void{
      this.livreService.getLivre().subscribe(
        (response:Livre[])=>{
          this.livres = response
          console.log(response);
        },
        (error:HttpErrorResponse)=>{
          alert(error.message)
        }
      )
  }

  onAddLivre(addForm : NgForm){
    document.getElementById('add-livre-form').click();
    this.livreService.addLivre(addForm.value).subscribe(
      (response: Livre) => {
        console.log(response);
        this.getLivres();
        addForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        addForm.reset();
      }
    );
  }

  public onOpenModal(livre: Livre, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    if (mode === 'add') {
      button.setAttribute('data-target', '#addLivreModal');
    }
    if (mode === 'edit') {
      this.editLivre = livre;
      button.setAttribute('data-target', '#updateLivreModal');
    }
    if (mode === 'delete') {
      this.deleteLivre = livre;
      button.setAttribute('data-target', '#deleteLivreModal');
    }
    container.appendChild(button);
    button.click();
  }

  onUpdateLivre(livre: Livre):void{
    this.livreService.updateLivre(livre).subscribe(
      (response: Livre) => {
        console.log(response);
        this.getLivres();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  onDeleteLivre(id: number):void{
    this.livreService.deleteLivre(id).subscribe(
      (response: void) => {
        console.log(response);
        this.getLivres();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public searchLivre(key: string): void {
    console.log(key);
    const results: Livre[] = [];
    for (const livre of this.livres) {
      if (livre.auteur.toLowerCase().indexOf(key.toLowerCase()) !== -1
        || livre.titre.toLowerCase().indexOf(key.toLowerCase()) !== -1 ){
        results.push(livre);
      }
    }
    this.livres = results;
    if (results.length === 0 || !key) {
      this.getLivres();
    }
  }


}
