
import {HttpEvent, HttpEventType, HttpResponse,HttpErrorResponse} from "@angular/common/http";
import {error} from "@angular/compiler-cli/src/transformers/util";
import {NgForm} from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Dictionnaire } from 'src/app/be/Dictionnaire';
import { DictionnaireService } from 'src/app/service/dictionnaire.service';

@Component({
  selector: 'app-dictionnaire',
  templateUrl: './dictionnaire.component.html',
  styleUrls: ['./dictionnaire.component.css']
})
export class DictionnaireComponent implements OnInit {
  public dictionnaires:Dictionnaire[];
  public editDictionnaire:Dictionnaire;
  public deleteDictionnaire: Dictionnaire;
  constructor(private dictionnaireService:DictionnaireService){}
  ngOnInit(): void {
    this.getDictionnaire();
  }

  getDictionnaire(): void {
    this.dictionnaireService.getDictionnaire().subscribe(
      (response:Dictionnaire[])=>{
        this.dictionnaires = response
        console.log(response);
      },
      (error:HttpErrorResponse)=>{
        alert(error.message)
      }
    )
  }

  onAddDictionnaire(addForm : NgForm){
    document.getElementById('add-dictionnaire-form').click();
    this.dictionnaireService.addDictionnaire(addForm.value).subscribe(
      (response: Dictionnaire) => {
        console.log(response);
        this.getDictionnaire();
        addForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        addForm.reset();
      }
    );
  }

  public onOpenModal(dictionnaire: Dictionnaire, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    if (mode === 'add') {
      button.setAttribute('data-target', '#addDictionnaireModal');
    }
    if (mode === 'edit') {
      this.editDictionnaire = dictionnaire;
      button.setAttribute('data-target', '#updateDictionnaireModal');
    }
    if (mode === 'delete') {
      this.deleteDictionnaire = dictionnaire;
      button.setAttribute('data-target', '#deleteProductModal');
    }
    container.appendChild(button);
    button.click();
  }


  onUpdateDictionnaire(dictionnaire: Dictionnaire):void{
    this.dictionnaireService.updateDictionnaire(dictionnaire).subscribe(
      (response: Dictionnaire) => {
        console.log(response);
        this.getDictionnaire();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  onDeleteDictionnaire(id: number):void{
    this.dictionnaireService.deleteDictionnaire(id).subscribe(
      (response: void) => {
        console.log(response);
        this.getDictionnaire();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public searchDictinnaire(key: string): void {
    console.log(key);
    const results: Dictionnaire[] = [];
    for (const dictionnaire of this.dictionnaires) {
      if (dictionnaire.titre.toLowerCase().indexOf(key.toLowerCase()) !== -1
      || dictionnaire.langue.toLowerCase().indexOf(key.toLowerCase()) !== -1 ){
        results.push(dictionnaire);
      }
    }
    this.dictionnaires = results;
    if (results.length === 0 || !key) {
      this.getDictionnaire();
    }
  }

}
