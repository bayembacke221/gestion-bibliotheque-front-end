import {Component, OnInit} from '@angular/core';
import {Revue} from "../../be/Revue";
import {RevueService} from "../../service/revue.service";
import {Dictionnaire} from "../../be/Dictionnaire";
import {HttpErrorResponse} from "@angular/common/http";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-revue',
  templateUrl: './revue.component.html',
  styleUrls: ['./revue.component.css']
})
export class RevueComponent implements OnInit{
  public revues: Revue[];
  public editRevue:Revue;
  public deleteRevue:Revue;

  constructor(private revueService:RevueService) {
  }
  ngOnInit(): void {
    this.getRevue();
  }

  getRevue(): void {
    this.revueService.getRevue().subscribe(
      (response:Revue[])=>{
        this.revues = response
        console.log(response);
      },
      (error:HttpErrorResponse)=>{
        alert(error.message)
      }
    )
  }

  onAddRevue(addForm : NgForm){
    document.getElementById('add-revue-form').click();
    this.revueService.addRevue(addForm.value).subscribe(
      (response: Revue) => {
        console.log(response);
        this.getRevue();
        addForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        addForm.reset();
      }
    );
  }

  public onOpenModal(revue: Revue, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    if (mode === 'add') {
      button.setAttribute('data-target', '#addRevueModal');
    }
    if (mode === 'edit') {
      this.editRevue = revue;
      button.setAttribute('data-target', '#updateDictionnaireModal');
    }
    if (mode === 'delete') {
      this.deleteRevue = revue;
      button.setAttribute('data-target', '#deleteProductModal');
    }
    container.appendChild(button);
    button.click();
  }


  onUpdateRevue(revue: Revue):void{
    this.revueService.updateRevue(revue).subscribe(
      (response: Revue) => {
        console.log(response);
        this.getRevue();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  onDeleteRevue(id: number):void{
    this.revueService.deleteRevue(id).subscribe(
      (response: void) => {
        console.log(response);
        this.getRevue();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public searchRevue(key: string): void {
    console.log(key);
    const results: Revue[] = [];
    for (const revue of this.revues) {
      if (revue.titre.toLowerCase().indexOf(key.toLowerCase()) !== -1){
        results.push(revue);
      }
    }
    this.revues = results;
    if (results.length === 0 || !key) {
      this.getRevue();
    }
  }


}
